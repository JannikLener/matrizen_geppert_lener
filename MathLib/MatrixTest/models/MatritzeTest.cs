﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Matrix.models;

namespace MatrixTest.models
{
    [TestFixture]
    class MatritzeTest
    {
        private const double delta = 0.0001;

        [Test]
        public void MatritzeShouldInitializeElementCorrectly()
        {
            //Arrange - Act
            Matritze m = new Matritze();

            //Assert
            Assert.AreEqual(new double[5,5], m.Points);
        }

        [TestCase(1,1)]
        [TestCase(2,1)]
        [TestCase(1,2)]
        [TestCase(6,4)]
        public void MatritzeShouldInitiallizeElementCorrectly(int row, int column)
        {
            //Arrange - Act
            Matritze m = new Matritze(row, column);

            //Assert
            Assert.AreEqual(new double[row, column], m.Points);
        }

        [TestCase(0, 0)]
        [TestCase(-1, -1)]
        public void MaritzeShouldThrowExceptionWhenNullOrNegativ(int row, int column)
        {
            //Arrange
            //Act
            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => { Matritze result = new Matritze(row, column); });
        }

        [TestCase(0,0,1)]
        [TestCase(0,1,2)]
        [TestCase(0,2,3)]
        [TestCase(1,2,6)]
        public void IndexerShouldGiveRightValue(int row, int column, double value)
        {
            Matritze index = new Matritze(2,3);
            index.Points = new double[,] { { 1,2,3 },{ 4,5,6 } };


            Assert.AreEqual(index[row, column],value);
        }

        [TestCase(1,2,3,4,5,6, 7,8,9,10,11,12, 58,64,139,154)]
        [TestCase(4,-5,-1,22,0,0, 1,0,-43,9,-2,3, 221,-48,22,0)]
        public void MultiplyShouldCalculateCorrectValuesWhenMatritzAreEqualyBig(double z1, double z2, double z3, double z4, double z5, double z6, double z7, 
            double z8, double z9, double z10, double z11, double z12, double z13, double z14, double z15, double z16)
        {
            //Arrange
            Matritze m1 = new Matritze(2, 3);
            m1.Points = new double[,]
            {
                {z1,z2,z3},
                {z4,z5,z6 }
            };
            Matritze m2 = new Matritze(3, 2);
            m2.Points = new double[,]
            {
                {z7,z8},
                {z9, z10},
                {z11,z12 }
            };
            Matritze mcorrect = new Matritze(2, 2);
            mcorrect.Points = new double[,]
            {
                {z13,z14},
                {z15,z16}
            };
            //Act
            Matritze mresult = m1 * m2;

            //Assert
            Assert.AreEqual(mcorrect.Points, mresult.Points);
        }

        [TestCase(4,-5,-1,22,0,0,33,12,-9, 1,0,-43,9,-2,3, 221,-48,22,0,-465,81)]
        public void MultiplyShouldCalculateCorrectValuesWhenMatritzAreNotEqualyBig(double z1, double z2, double z3, double z4, double z5, double z6, double z7,
            double z8, double z9, double z10, double z11, double z12, double z13, double z14, double z15, double z16, double z17, double z18, double z19, double z20, double z21)
        {
            //Arrange
            Matritze m1 = new Matritze(3, 3);
            m1.Points = new double[,]
            {
                {z1,z2,z3},
                {z4,z5,z6},
                {z7,z8,z9}
            };
            Matritze m2 = new Matritze(3, 2);
            m2.Points = new double[,]
            {
                {z10,z11},
                {z12,z13},
                {z14,z15}
            };
            Matritze mcorrect = new Matritze(3, 2);
            mcorrect.Points = new double[,]
            {
                {z16,z17},
                {z18,z19},
                {z20,z21}
            };
            //Act
            Matritze mresult = m1 * m2;

            //Assert
            Assert.AreEqual(mcorrect.Points, mresult.Points);
        }

        [Test]
        public void MultiplyShouldThrowExceptionWhenCloumnAndRowAreNotEqual()
        {
            //Arrange
            Matritze m1 = new Matritze(3, 3);
            m1.Points = new double[,]
            {
                {1,2,3},
                {4,5,6},
                {7,8,9}
            };
            Matritze m2 = new Matritze(2, 2);
            m2.Points = new double[,]
            {
                {10,11},
                {12,13},
            };

            //Act
            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => { Matritze result = m1 * m2; });
        }

        [TestCase(1, 2, 3, 4, 5, 6  ,  1, 2, 3, 4, 5, 6  ,  2, 4, 6, 8, 10, 12)]
        [TestCase(-1, -2, 3, 0, 5, 6  ,  1, -2, -3, 4, 5, 6  ,  0, -4, 0, 4, 10, 12)]
        public void AddShouldCalculateCorrectValuesMatritzAreEqualyBig(double a1, double a2, double a3, double a4, double a5, double a6, double b1, double b2, double b3, 
            double b4, double b5, double b6, double res1, double res2, double res3, double res4, double res5, double res6)
        {
            //Arrange
            Matritze m1 = new Matritze(2, 3);
            m1.Points = new double[,] { { a1, a2, a3 }, { a4, a5, a6 } };
            Matritze m2 = new Matritze(2, 3);
            m2.Points = new double[,] { { b1, b2, b3 }, { b4, b5, b6 } };
            Matritze mcorrect = new Matritze(2, 3);
            mcorrect.Points = new double[,] { { res1, res2, res3 }, { res4, res5, res6 } };

            //Act
            Matritze mresult = m1 + m2;

            //Assert
            Assert.AreEqual(mcorrect.Points, mresult.Points);
        }

        [TestCase(4,-5,-1,22,0,0,33,12,-9, 1,0,-43,9,-2,3, 5,-5,-1,-21,9,0,31,15,-9)]
        public void AddShouldCalculateCorrectValuesMatritzAreEqualyBig(double a1, double a2, double a3, double a4, double a5, double a6, double a7, double a8, double a9, 
            double b1, double b2, double b3, double b4, double b5, double b6, double res1, double res2, double res3, double res4, double res5, double res6, double res7, 
            double res8, double res9)
        {
            //Arrange
            Matritze m1 = new Matritze(3, 3);
            m1.Points = new double[,]
            {
                {a1,a2,a3},
                {a4,a5,a6},
                {a7,a8,a9}
            };
            Matritze m2 = new Matritze(3, 2);
            m2.Points = new double[,]
            {
                {b1,b2},
                {b3,b4},
                {b5,b6}
            };
            Matritze mcorrect = new Matritze(3, 3);
            mcorrect.Points = new double[,]
            {
                {res1,res2,res3},
                {res4,res5,res6},
                {res7,res8,res9}
            };

            //Act
            Matritze mresult = m1 + m2;

            //Assert
            Assert.AreEqual(mcorrect.Points, mresult.Points);
        }

        [TestCase(1,2,3,4,5,6, 1,1,2,3,2,1, 0,1,1,1,3,5)]
        [TestCase(-1,2,3,4,5,6 , -1,-1,2,3,2,1 , 0,3,1,1,3,5)]
        public void SubShouldCalculateCorrectValuesMatritzAreEqualyBig(double a1, double a2, double a3, double a4, double a5, double a6, double b1, double b2, double b3,
            double b4, double b5, double b6, double res1, double res2, double res3, double res4, double res5, double res6)
        {
            //Arrange
            Matritze m1 = new Matritze(2, 3);
            m1.Points = new double[,] { { a1, a2, a3 }, { a4, a5, a6 } };
            Matritze m2 = new Matritze(2, 3);
            m2.Points = new double[,] { { b1, b2, b3 }, { b4, b5, b6 } };
            Matritze mcorrect = new Matritze(2, 3);
            mcorrect.Points = new double[,] { { res1, res2, res3 }, { res4, res5, res6 } };

            //Act
            Matritze mresult = m1 - m2;

            //Assert
            Assert.AreEqual(mcorrect.Points, mresult.Points);
        }

        [TestCase(4,-5,-1,22,0,0,33,12,-9, 1,0,-43,9,-2,3, 3,-5,-1,65,-9,0,35,9,-9)]
        public void SubShouldCalculateCorrectValuesMatritzAreEqualyBig(double a1, double a2, double a3, double a4, double a5, double a6, double a7, double a8, double a9,
            double b1, double b2, double b3, double b4, double b5, double b6, double res1, double res2, double res3, double res4, double res5, double res6, double res7,
            double res8, double res9)
        {
            //Arrange
            Matritze m1 = new Matritze(3, 3);
            m1.Points = new double[,]
            {
                {a1,a2,a3},
                {a4,a5,a6},
                {a7,a8,a9}
            };
            Matritze m2 = new Matritze(3, 2);
            m2.Points = new double[,]
            {
                {b1,b2},
                {b3,b4},
                {b5,b6}
            };
            Matritze mcorrect = new Matritze(3, 3);
            mcorrect.Points = new double[,]
            {
                {res1,res2,res3},
                {res4,res5,res6},
                {res7,res8,res9}
            };

            //Act
            Matritze mresult = m1 - m2;

            //Assert
            Assert.AreEqual(mcorrect.Points, mresult.Points);
        }

        [TestCase(3,3,3, 2,3,4, 5,6,7)]
        [TestCase(3,3,3, -2,-3,-4, 1,0,-1)]
        public void TranslationShouldMoveOnePointCorrectly(double x, double y, double z, double movex, double movey, double movez, double xres, double yres, double zres)
        {
            //Arrange
            Matritze m = new Matritze(4, 1);
            m.Points = new double[,] { { x }, { y }, { z }, { 1 } };

            Matritze mcorrect = new Matritze(4, 1);
            mcorrect.Points = new double[,] { { xres }, { yres }, { zres }, { 1 } };

            //Act
            //Assert
            Assert.AreEqual(mcorrect.Points, m.Translation(movex,movey,movez).Points);
        }


        [TestCase(3,3,3,10,10,10,5,6,7, 2,3,4, 5,6,7,12,13,14,7,9,11)]
        public void TranslationShouldMoveManyPointsCorrectly(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, 
            double movex, double movey, double movez, double x1res, double y1res, double z1res, double x2res, double y2res, double z2res, double x3res, double y3res, 
            double z3res)
        {
            //Arrange
            Matritze m = new Matritze(4, 3);
            m.Points = new double[,] 
            { 
                { x1, x2, x3 }, 
                { y1, y2, y3 }, 
                { z1, z2, z3 }, 
                { 1, 1, 1 }
            };

            Matritze mcorrect = new Matritze(4, 3);
            mcorrect.Points = new double[,] 
            { 
                { x1res, x2res, x3res }, 
                { y1res, y2res, y3res }, 
                { z1res, z2res, z3res }, 
                { 1, 1, 1 }
            };

            //Act
            //Assert
            Assert.AreEqual(mcorrect.Points, m.Translation(movex, movey, movez).Points);
        }

        [TestCase(5,0,5, 0,0,0, 5,0,5)]
        [TestCase(5,0,5, 90,0,0, 5,-5,0)]
        [TestCase(5,0,5, 0,90,0, 5,0,-5)]
        [TestCase(5,0,5, 0,0,90, 0,5,5)]
        [TestCase(5,0,5, 90,90,90, 5,0,-5)]
        public void RotateShouldRotateAnObjectCorrectly(double x, double y, double z, double anglex, double angley, double anglez, double xres, double yres, double zres)
        {
            Matritze m = new Matritze(3, 1);
            m.Points = new double[,] { { x }, { y }, { z }};

            Matritze mcorrect = new Matritze(3, 1);
            mcorrect.Points = new double[,] { { xres }, { yres }, { zres }};

            //Act
            //Assert
            Assert.AreEqual(mcorrect.Points[0, 0], m.Rotate(anglex, angley, anglez).Points[0, 0], delta);
            Assert.AreEqual(mcorrect.Points[1, 0], m.Rotate(anglex, angley, anglez).Points[1, 0], delta);
            Assert.AreEqual(mcorrect.Points[2, 0], m.Rotate(anglex, angley, anglez).Points[2, 0], delta);
        }

        [TestCase(0,0,0, 2,4,3, 0,0,5, 90,0,0, 0,0,0, 3,4,-2, 5,0,0)]
        public void RotateShouldRotateManyObjectsCorrectly(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, 
            double anglex, double angley, double anglez, double x1res, double y1res, double z1res, double x2res, double y2res, double z2res, double x3res, double y3res,
            double z3res)
        {
            Matritze m = new Matritze(3, 3);
            m.Points = new double[,]
            {
                { x1, x2, x3 },
                { y1, y2, y3 },
                { z1, z2, z3 }
            };

            Matritze mcorrect = new Matritze(3, 3);
            mcorrect.Points = new double[,]
            {
                { x1res, x2res, x3res },
                { y1res, y2res, y3res },
                { z1res, z2res, z3res }
            };

            //Act
            //Assert
            Assert.AreEqual(mcorrect.Points[0, 0], m.Rotate(anglex, angley, anglez).Points[0, 0], delta);
            Assert.AreEqual(mcorrect.Points[1, 0], m.Rotate(anglex, angley, anglez).Points[1, 0], delta);
            Assert.AreEqual(mcorrect.Points[2, 0], m.Rotate(anglex, angley, anglez).Points[2, 0], delta);
        }


        [TestCase(2,3,4, 5,6,7, 8,9,10 , 2,3,4 , 4,9,16 ,10,18,28, 16,27,40)]
        public void ScaleDifferentShouldScaleWitchXYZ(double x1, double y1, double z1,
            double x2,double y2, double z2,
            double x3, double y3, double z3,
            double xfactor, double yfactor, double zfactor,
            double x1res, double y1res, double z1res,
            double x2res, double y2res, double z2res,
            double x3res, double y3res, double z3res)
        {
            Matritze m = new Matritze(3, 3);
            m.Points = new double[,]
            {
                { x1, x2, x3 },
                { y1, y2, y3 },
                { z1, z2, z3 }
            };

            Matritze mcorrect = new Matritze(3, 3);
            mcorrect.Points = new double[,]
            {
                { x1res, x2res, x3res },
                { y1res, y2res, y3res },
                { z1res, z2res, z3res }
            };

            Assert.AreEqual(mcorrect.Points, m.ScaleDifferent(m,xfactor, yfactor, zfactor).Points);
        }

        [TestCase(2, 3, 4, 5, 6, 7, 8, 9, 10, 2, -3, 0, 4, -9, 0, 10, -18, 0, 16, -27, 0)]
        public void ScaleShouldThrowExceptionIfXYZFactorsAreNegative(double x1, double y1, double z1,
            double x2, double y2, double z2,
            double x3, double y3, double z3,
            double xfactor, double yfactor, double zfactor,
            double x1res, double y1res, double z1res,
            double x2res, double y2res, double z2res,
            double x3res, double y3res, double z3res)
        {
            Matritze m = new Matritze(3, 3);
            m.Points = new double[,]
            {
                { x1, x2, x3 },
                { y1, y2, y3 },
                { z1, z2, z3 }
            };
            Assert.Throws<ArgumentOutOfRangeException>(() => { Matritze result = m.ScaleDifferent(m, xfactor, yfactor, zfactor); });

        }

        [TestCase(1,2,3 , 4,5,6 , 7,8,9 , 10 , 10,20,30,40,50,60,70,80,90)]
        public void ScaleSameShouldScaleWithOneFactor(double x1, double y1, double z1,
           double x2, double y2, double z2,
           double x3, double y3, double z3,
           double factor, 
           double x1res, double y1res, double z1res,
           double x2res, double y2res, double z2res,
           double x3res, double y3res, double z3res)
        {
            Matritze m = new Matritze(3, 3);
            m.Points = new double[,]
            {
                { x1, x2, x3 },
                { y1, y2, y3 },
                { z1, z2, z3 }
            };

            Matritze mcorrect = new Matritze(3, 3);
            mcorrect.Points = new double[,]
            {
                { x1res, x2res, x3res },
                { y1res, y2res, y3res },
                { z1res, z2res, z3res }
            };

            Assert.AreEqual(mcorrect.Points, m.ScaleSame(m,factor).Points);
        }


    }
}
