﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix.models
{
    public class Matritze
    {
        //Fields
        private int row, column;

        //Properties
        public double[,] Points { get; set; }
        public int Row
        {
            get
            {
                return this.row;
            }
            set
            {
                if (value > 0)
                {
                    this.row = value;
                }
            }
        }
        public int Column
        {
            get
            {
                return this.column;
            }
            set
            {
                if (value > 0)
                {
                    this.column = value;
                }
            }
        }
        public double this[int row, int column]
        {
            get { return Points[row, column]; }
            set { Points[row, column] = value; }
        }

        //other Methods
        public static Matritze operator +(Matritze b, Matritze c)
        {
            return b.Add(c);
        }
        public static Matritze operator -(Matritze b, Matritze c)
        {
            return b.Sub(c);
        }
        public static Matritze operator *(Matritze b, Matritze c)
        {
            return b.Multiply(c);
        }

        public Matritze Multiply(Matritze m)
        {
            Matritze mresult = new Matritze(this.Row,m.Column);
            if (this.Column != m.Row)
            {
                throw new ArgumentOutOfRangeException();
            }
            for (int r = 0; r < mresult.Row; r++)
            {
                for (int c = 0; c < mresult.Column; c++)
                {
                    for (int i = 0; i < this.Column; i++)
                    {
                        mresult[r, c] += (this[r, i] * m[i, c]);
                    }
                }
            }
            return mresult;
        }

        public Matritze Add(Matritze m)
        {
            //Größe der Ergebnis Matrix ermitteln
            int resColumns, resRows;
            if (this.Row > m.Row)
            {
                resRows = this.Row;
            }
            else
            {
                resRows = m.Row;
            }
            if (this.Column > m.Column)
            {
                resColumns = this.Column;
            }
            else
            {
                resColumns = m.Column;
            }

            Matritze mresult = new Matritze (resRows, resColumns);

            //Addition
            for (int r = 0; r < mresult.Row; r++)
            {
                for (int c = 0; c < mresult.Column; c++)
                {
                    double summand1 = 0, summand2 = 0;
                    if ((r<this.Row)&&(c<this.Column))
                    {
                        summand1 = this.Points[r, c];
                    }
                    if ((r < m.Row) && (c < m.Column))
                    {
                        summand2 = m.Points[r, c];
                    }
                    mresult[r, c] = summand1 + summand2;
                }
            }

            return mresult;

        }
        public Matritze Sub(Matritze m)
        {
            //Größe der Ergebnis Matrix ermitteln
            int resColumns, resRows;
            if (this.Row > m.Row)
            {
                resRows = this.Row;
            }
            else
            {
                resRows = m.Row;
            }
            if (this.Column > m.Column)
            {
                resColumns = this.Column;
            }
            else
            {
                resColumns = m.Column;
            }
            Matritze mresult = new Matritze(resRows, resColumns);

            //Subtraktion
            for (int r = 0; r < mresult.Row; r++)
            {
                for (int c = 0; c < mresult.Column; c++)
                {
                    double sub1 = 0, sub2 = 0;
                    if ((r < this.Row) && (c < this.Column))
                    {
                        sub1 = this.Points[r, c];
                    }
                    if ((r < m.Row) && (c < m.Column))
                    {
                        sub2 = m.Points[r, c];
                    }
                    mresult[r, c] = sub1 - sub2;
                }
            }

            return mresult;

        }

        public Matritze Translation(double x, double y, double z)
        {
            //Überprüfen ob Matrix überhaupt verschoben werden kann
            if(this.Row != 4)
            {
                throw new ArgumentOutOfRangeException();
            }

            //Translationsmatrix erzeugen
            Matritze m = new Matritze(4, 4);
            m.Points = new double[,]
            {
                {1,0,0,x},
                {0,1,0,y},
                {0,0,1,z},
                {0,0,0,1}
            };

            //return m.Multiply(this);
            return m * this;
        }

        public Matritze Rotate(double x, double y, double z)
        {
            //Überprüfen ob Matrix überhaupt gedreht werden kann
            if (this.Row != 3)
            {
                throw new ArgumentOutOfRangeException();
            }

            //Winkel umrechnen
            x = x * Math.PI / 180;
            y = y * Math.PI / 180;
            z = z * Math.PI / 180;

            //Rotationsmatrix erzeugen
            Matritze mx = new Matritze(3, 3);
            Matritze my = new Matritze(3, 3);
            Matritze mz = new Matritze(3, 3);
            mx.Points = new double[,]
            {
                {1,0,0},
                {0,Math.Cos(x),-Math.Sin(x)},
                {0,Math.Sin(x),Math.Cos(x)},
            };
            my.Points = new double[,]
            {
                {Math.Cos(y),0,Math.Sin(y)},
                {0,1,0},
                {-Math.Sin(y),0,Math.Cos(y)},
            };
            mz.Points = new double[,]
            {
                {Math.Cos(z),-Math.Sin(z),0},
                {Math.Sin(z),Math.Cos(z),0},
                { 0,0,1}
            };

            //Berechnunen und zurückgeben
            return mz.Multiply(my.Multiply(mx.Multiply(this)));
        }

        //Ctor
        public Matritze() : this(5, 5) { }
        public Matritze(int row, int column)
        {
            if ((column < 1) || (row < 1))
            {
                throw new ArgumentOutOfRangeException();
            }
            Points = new double[row, column];
            Row = row;
            Column = column;
        }


     
        public Matritze ScaleDifferent(Matritze m ,double xFactor, double yFactor, double zFactor)
        {
            Matritze scaleres = new Matritze();
            if(xFactor >= 0 && yFactor >= 0 && zFactor >= 0)
            {
                for (int c = 0; c < m.column; c++)
                {
                    this.Points[0, c] = this.Points[0, c] * xFactor;
                }
                for(int c = 0; c < m.Column; c++)
                {
                    this.Points[1, c] = this.Points[1, c] * yFactor;
                }
                for (int c = 0; c < m.Column; c++)
                {
                    this.Points[2, c] = this.Points[2, c] * zFactor;
                }
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
            return this;
        }

        public Matritze ScaleSame(Matritze m, double factor)
        {
            Matritze scaleres = new Matritze();
            if (factor != 1)
            {
                for (int c = 0; c < m.column; c++)
                {
                    this.Points[0, c] = this.Points[0, c] * factor;
                }
                for (int c = 0; c < m.Column; c++)
                {
                    this.Points[1, c] = this.Points[1, c] * factor;
                }
                for (int c = 0; c < m.Column; c++)
                {
                    this.Points[2, c] = this.Points[2, c] * factor;
                }
            }
            return this;
        }

        //ToString()
        public override string ToString()
        {
            string s = "";

            for (int i = 0; i < this.Row; i++)
            {
                for (int z = 0; z < this.Column; z++)
                {
                    s += "||";
                    s += Points[i, z];
                }
                s += "||";
                s += "\n";
            }

            return s;
        }
    }
}
